import sys
from json import dump

with open(sys.argv[1] + "/" + sys.argv[2] + "/" + sys.argv[2] + ".json", "w") as write_file:
    dump(eval(sys.argv[3]), write_file, indent=5, separators=(',', ': '))

